
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! This code will generate a grid of initial condition 
! files that can be used to run CAMB
!
! From this grid one can then calculate the Fisher matrix derivatives
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! The code is set for 9 parameters
!
! p = { w0, wa, om_DE, om_mc^2, om_bh^2, ns, As, alpha_s, tau}
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

program CAMB_2LPT_GenParamFiles

  implicit none

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! CAMB INPUT PARAMETERS

  !-------------
  ! What to do: 
  character(len=1) :: get_scalar_cls = 'T'    ! scalar perturbations
  character(len=1) :: get_vector_cls = 'F'    ! vector
  character(len=1) :: get_tensor_cls = 'F'    ! tensor
  character(len=1) :: get_transfer   = 'T'    ! transfer functions
  character(len=1) :: do_lensing     = 'F'    ! do lensing of CMB

  integer :: do_nonlinear  =  0               ! use nonlinear model for power spectrum

  !-------------
  ! MULTIPOLE RANGE
  integer :: l_max_scalar      = 2000         
  integer :: k_eta_max_scalar  = 10000
  integer :: l_max_tensor      = 1500
  integer :: k_eta_max_tensor  = 3000

  !-------------
  ! FLRW COSMOLOGICAL MODEL 
  ! Main cosmological parameters, neutrino masses are assumed degenerate
  ! If use_phyical set phyiscal densities in baryone, CDM and neutrinos + Omega_k

  character(len=1) :: use_physical   = 'T'

  real(8) :: ombh2     ! density parameters * h^2
  real(8) :: omch2
  real(8) :: omnuh2 = 0.0
  real(8) :: omk    = 0.0     
 
  real(8) :: hubble                           ! hubble parameter [km/s/Mpc]
  
  real(8) :: w=-1.0                           ! w constant (redundant function)
  character(len=1) :: usew0wa = 'T'           ! Dark energy flag 
  real(8) :: w0, wa                           ! if usew0wa = T: w(a)=w0+(1-a)wa 
  character(len=120) :: wafile = 'junk.dat'   ! if usew0wa = F, read (a,w) from user-supplied file

  real(8) :: cs2_lam        = 1               ! constant comoving sound speed of the dark energy (1=quintessence)

  real(8) :: temp_cmb           = 2.726       ! CMB Temp in Kelvin
  real(8) :: helium_fraction    = 0.24        ! Helium fraction

  !-------------
  ! NEUTRINOS
  ! #massless_neutrinos is the effective number (for QED + non-instantaneous decoupling)
  
  real(8) :: massless_neutrinos  = 3.04
  real(8) :: massive_neutrinos   = 0       ! massive neutrinos 
  integer :: nu_mass_eigenstates = 1       ! Neutrino mass splittings

  !#nu_mass_degeneracies = 0 sets nu_mass_degeneracies = massive_neutrinos
  !#otherwise should be an array
  !#e.g. for 3 neutrinos with 2 non-degenerate eigenstates, nu_mass_degeneracies = 2 1
  integer :: nu_mass_degeneracies = 0  

  !#Fraction of total omega_nu h^2 accounted for by each eigenstate, eg. 0.5 0.5
  integer :: nu_mass_fractions = 1

  !-------------
  ! INITIAL POWER SPECTRUM
  !#Initial power spectrum, amplitude, spectral index and running. Pivot k in Mpc^{-1}.
  integer :: initial_power_num         = 1
  real(8) :: pivot_scalar              = 0.05
  real(8) :: pivot_tensor              = 0.05
  real(8) :: scalar_amp(1)            
  real(8) :: scalar_spectral_index(1)  
  real(8) :: scalar_nrun(1)
  real(8) :: tensor_spectral_index(1)  = 0
  !#ratio is that of the initial tens/scal power spectrum amplitudes
  real(8) :: initial_ratio(1)          = 1
  !#note vector modes use the scalar settings above

  !-------------
  ! REIONIZATION
  !#Reionization, ignored unless reionization = T, re_redshift measures where x_e=0.5
  character(len=1) :: reionization      = 'T'
  character(len=1) :: re_use_optical_depth = 'T'
  real(8) :: re_optical_depth     = 0.0952

  !#If re_use_optical_depth = F then use following, otherwise ignored
  real(8) :: re_redshift          = 11
  !#width of reionization transition. CMBFAST model was similar to re_delta_redshift~0.5.
  real(8) :: re_delta_redshift    = 1.5
  !#re_ionization_frac=-1 sets to become fully ionized using YHe to get helium contribution
  !#Otherwise x_e varies from 0 to re_ionization_frac
  real(8) :: re_ionization_frac   = -1
  
  !#RECFAST 1.4 recombination parameters
  real(8) :: RECFAST_fudge = 1.14
  real(8) :: RECFAST_fudge_He = 0.86
  integer :: RECFAST_Heswitch = 6

  !-------------
  ! PERTURBATION MODEL

  !#Initial scalar perturbation mode (adiabatic=1, CDM iso=2, Baryon iso=3, 
  !# neutrino density iso =4, neutrino velocity iso = 5) 
  integer :: initial_condition   = 1

  !#If above is zero, use modes in the following (totally correlated) proportions
  !#Note: we assume all modes have the same initial power spectrum
  integer :: initial_vector(5) = (/-1, 0, 0, 0, 0 /)

  !#For vector modes: 0 for regular (neutrino vorticity mode), 1 for magnetic
  integer:: vector_mode = 0

  !-------------
  !#Normalization
  character(len=1) ::    COBE_normalize = 'F'
  !##CMB_outputscale scales the output Cls
  !#To get MuK^2 set realistic initial amplitude (e.g. scalar_amp(1) = 2.3e-9 above) and
  !#otherwise for dimensionless transfer functions set scalar_amp(1)=1 and use
  !#CMB_outputscale = 1
  real(8) :: CMB_outputscale = 7.4311e12   ! Tcmb**2 * 1.0e12 

  !-------------
  ! TRANSFER FUNCTIONS

  !#Transfer function settings, transfer_kmax=0.5 is enough for sigma_8
  !#transfer_k_per_logint=0 sets sensible non-even sampling; 
  !#transfer_k_per_logint=5 samples fixed spacing in log-k
  !#transfer_interp_matterpower =T produces matter power in regular interpolated grid in log k; 
  !# use transfer_interp_matterpower =F to output calculated values (e.g. for later interpolation)

  character(len=1) ::    transfer_high_precision = 'F'

  real(8) :: transfer_kmax           = 100.0

  integer :: transfer_k_per_logint   = 40 

  integer :: transfer_num_redshifts  = 1

  character(len=1) ::    transfer_interp_matterpower = 'T'

  real(8) :: transfer_redshift(1)    = 0.0

  character(len=120) :: transfer_filename(1)    

  !#Matter power spectrum output against k/h in units of h^{-3} Mpc^3
  character(len=120) :: transfer_matterpower(1) 

  !-------------
  ! OUTPUT FILES
  !#Output files not produced if blank. make camb_fits to use use the FITS setting.
  character(len=220) :: output_rootCAMB
  character(len=220) :: scalar_output_file       
  character(len=220) :: vector_output_file       
  character(len=220) :: tensor_output_file       
  character(len=220) :: total_output_file        
  character(len=220) :: lensed_output_file       
  character(len=220) :: lensed_total_output_file 
  character(len=220) :: FITS_filename            

  !-------------
  ! OPTIONAL PARAMETERS

  !##Optional parameters to control the computation speed,accuracy and feedback

  !#If feedback_level > 0 print out useful information computed about the model
  integer :: feedback_level = 1

  !# 1: curved correlation function, 2: flat correlation function, 3: inaccurate harmonic method
  integer :: lensing_method = 1
  character(len=1) ::   accurate_BB = 'F'

  !#massive_nu_approx: 0 - integrate distribution function
  !#                   1 - switch to series in velocity weight once non-relativistic
  !#                   2 - use fast approximate scheme (CMB only- accurate for light neutrinos)
  !#                   3 - intelligently use the best accurate method
  integer ::   massive_nu_approx = 3

  !#Whether you are bothered about polarization. 
  character(len=1) ::  accurate_polarization   = 'T'

  !#Whether you are bothered about percent accuracy on EE from reionization
  character(len=1) ::  accurate_reionization   = 'T'

  !#whether or not to include neutrinos in the tensor evolution equations
  character(len=1) ::  do_tensor_neutrinos     = 'F'
  
  !#Whether to turn off small-scale late time radiation hierarchies (save time,v. accurate)
  character(len=1) :: do_late_rad_truncation   = 'T'
  
  !#Computation parameters
  !#if number_of_threads=0 assigned automatically
  integer :: number_of_threads       = 0
  
  !#Default scalar accuracy is about 0.3% (except lensed BB). 
  !#For 0.1%-level try accuracy_boost=2, l_accuracy_boost=2.
  
  !#Increase accuracy_boost to decrease time steps, use more k values,  etc.
  !#Decrease to speed up at cost of worse accuracy. Suggest 0.8 to 3.
  real(8) :: accuracy_boost          = 2.0
  
  !#Larger to keep more terms in the hierarchy evolution. 
  real(8) :: l_accuracy_boost        = 2.0
  
  !#Increase to use more C_l values for interpolation.
  !#Increasing a bit will improve the polarization accuracy at l up to 200 -
  !#interpolation errors may be up to 3%
  !#Decrease to speed up non-flat models a bit
  real(8) :: l_sample_boost          = 2.0
  
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! 2LPT INPUT PARAMETERS

  real(8) :: omm, omde, omb, omrad, omnu ! energy densities in various species

  real(8) :: zFNL, FNL                   ! non-gaussianity redshift and value

  integer :: Nmesh

  real(8) :: zout                        ! redshift at which ics generated

  real(8) :: Box

  character(len=120) :: MemFile

  character(len=120) :: FileBase

  character(len=120) :: OutputDir

  character(len=120) :: GlassFile

  integer :: GlassTileFac

  character(len=120) :: InputSpectrumFile

  integer :: NumFilesWrittenInParallel

  real(8) :: UnitLength_in_cm

  real(8) :: UnitMass_in_g

  real(8) :: UnitVelocity_in_cm_per_s

  integer :: nensemb, iensemb
  integer, parameter :: nensembMAX=20

  integer, dimension(nensembMAX) :: Seed

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  ! auxilliary variables 

  integer :: npar
  integer, parameter :: NPARMAX = 8

  integer :: par(NPARMAX), parlength

  character(len=220) :: string, outfileCAMB, outfile2LPT, outfile2LPTtmp

  integer :: iDE1, iDE2, iomDE, iomch2, iombh2, iOptDep
  integer :: iScalSpec, iScalAmp, iScalRun

  ! FIDUCIAL PARAMETERS

  real(8) :: w0_FID, wa_FID, hubble_FID
  real(8) :: omch2_FID, ombh2_FID, omk_FID, omDE_FID, omc_FID, omb_FID, omm_FID
  real(8) :: re_optical_depth_FID
  real(8) :: scalar_spectral_index_FID(1), scalar_amp_FID(1), scalar_nrun_FID(1)

  ! auxilliary parameters

  real(8) :: hlittle, ommh2

  integer :: imodel,ivar

  character(len=220) :: var1, var2, rootdir

  integer :: ii

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
  write(*,*) ' >>> running CAMBGenParamFiles <<<'
  write(*,*) '! The code is set to generate CAMB scripts for variation of 9 params'
  write(*,*) '! p = { w0, wa, om_DE, om_mc^2, om_bh^2, ns, As, alpha_s, tau}'
  write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  write(*,*) 'input ipars? [1=PLANCK2013; 2 = WMAP7]'
  read(*,*) imodel
  
  write(*,*) 'input vatiaions? [ 1 = 1%; 2 = By-Hand ]'
  read(*,*) ivar
  if(ivar>2.or.ivar<1) then
     write(*,*) 'ivar wrong value'
     stop
  endif

  write(*,*) 'input nensemb?'
  read(*,*) nensemb

  if(nensemb>nensembMAX) then
     write(*,*) 'nensemb>nensembMAX'
     stop
  endif

  do iensemb=1,nensemb
     Seed(iensemb)= 1000+iensemb
  enddo
  
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(imodel==1) then
     
     ! fiducial parameters that get varied
     ! Planck2013
     
     w0_FID = -1.0
     wa_FID = 0.0
     hubble_FID = 67.7700
     omch2_FID  = 0.11889
     ombh2_FID  = 0.022161
     omk_FID = 0.0
     scalar_spectral_index_FID(1) = 0.9611
     scalar_amp_FID(1) = 2.14818e-9           ! sig8=0.8
     scalar_nrun_FID(1) = 0.0
     
  elseif(imodel==2) then
     
     ! fiducial parameters that get varied  
     ! WMAP7

     w0_FID = -1.0
     wa_FID = 0.0
     hubble_FID = 70.4
     omch2_FID  = 0.1125
     ombh2_FID  = 0.0226
     omk_FID = 0.0
     scalar_spectral_index_FID(1) = 0.963
     scalar_amp_FID(1) = 2.173e-9           ! sig8=0.809
     scalar_nrun_FID(1) = 0.0
     
  endif

  ! derived parameters
  
  hlittle=hubble_FID/100.0
  
  omc_FID=omch2_FID/(hlittle)**2
  omb_FID=ombh2_FID/(hlittle)**2
  
  omm_FID=omc_FID+omb_FID
  
  omDE_FID = 1.0d0 - omk_FID - omm_FID 
     
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   do iDE1=1,3
   do iDE2=1,3
   do iomDE=1,3
   do iomch2=1,3
   do iombh2=1,3
   do iScalSpec=1,3
   do iScalAmp=1,3
   do iScalRun=1,3

     par(1)=iDE1-2        ! w0
     par(2)=iDE2-2        ! wa
     par(3)=iomDE-2       ! om_DE
     par(4)=iomch2-2      ! omch2
     par(5)=iombh2-2      ! ombh2
     par(6)=iScalSpec-2   ! Scalar spectral index
     par(7)=iScalAmp-2    ! Scalar amp
     par(8)=iScalRun-2    ! Scalar Running

     parlength = sum(par*par)

     if(parlength<2) then

        write(*,*) parlength

        if(ivar==1) then

           ! 1% Variations
           
           w0=w0_FID*(1.0+(iDE1-2)*0.01)              ! +-1%
           wa=wa_FID+(iDE2-2)*0.01                    ! +-0.01
           omde=omde_FID+(iomde-2)*0.01               ! +-1%
           omch2=omch2_FID*(1.0+(iomch2-2)*0.01)      ! +-1%
           ombh2=ombh2_FID*(1.0+(iombh2-2)*0.01)      ! +-1%
           
           scalar_spectral_index(1) = & 
                scalar_spectral_index_FID(1)* & 
                (1.0+(iScalSpec-2)*0.01)              ! +-1%      
           
           scalar_amp=scalar_amp_FID(1)* & 
                (1.0+(iScalAmp-2)*0.01)               ! +-1%  
           
           scalar_nrun(1)=scalar_nrun_FID(1)*(1.0) & 
                +(iScalRun-2)*0.01                    ! +-0.01
           
           
        elseif(ivar==2) then
           
           ! SET BY HAND
           
           w0=w0_FID*(1.0+(iDE1-2)*0.1)               ! +-10%
           wa=wa_FID+(iDE2-2)*0.2                     ! +-0.2
           omde=omde_FID+(iomde-2)*0.05               ! +-5%
           omch2=omch2_FID*(1.0+(iomch2-2)*0.05)      ! +-5%
           ombh2=ombh2_FID*(1.0+(iombh2-2)*0.05)      ! +-5%
           
           scalar_spectral_index(1) = & 
                scalar_spectral_index_FID(1)* & 
                (1.0+(iScalSpec-2)*0.05)              ! +-5%      
           
           scalar_amp=scalar_amp_FID(1)* & 
                (1.0+(iScalAmp-2)*0.1)               ! +-10%  
           
           scalar_nrun(1)=scalar_nrun_FID(1)*(1.0) & 
                +(iScalRun-2)*0.01                   ! +-0.01
           
        else
           write(*,*) 'ivar wrong'
           stop
        endif
        
        
        ommh2=omch2+ombh2
        omm=1.0-omDE

        hubble=100.0*sqrt(ommh2/omm)
  
        omb=ombh2/hubble/hubble

        omnu=omnuh2/hubble/hubble


        par=par+2

        write(string(1:8),'(8i1)') par(1),par(2),par(3),par(4),par(5),par(6),par(7),par(8)

        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ! SET THE CAMB INPUT PARAMETERS
        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        ! CAMB OUTPUT FILES
        
        if(imodel==1) then

           if(ivar==1) then
              outfileCAMB='CAMBparamfiles/Planck2013.Step_0.01.HighAcc.'//trim(string(1:8))//'.ini'
              output_rootCAMB = '/u/rosm/NBODY/Daemmerung/CAMB/CAMBdata/Planck2013.Step_0.01.HighAcc'
           elseif(ivar==2) then
              outfileCAMB='CAMBparamfiles/Planck2013.Step_ByHand.HighAcc.'//trim(string(1:8))//'.ini'
              output_rootCAMB = '/u/rosm/NBODY/Daemmerung/CAMB/CAMBdata/Planck2013.Step_ByHand.HighAcc'
           endif
           
        elseif(imodel==2) then
           
           if(ivar==1) then
              outfileCAMB='CAMBparamfiles/WMAP7-CAMB.Step_0.01.HighAcc.'//trim(string(1:8))//'.ini'
              output_rootCAMB = 'CAMBdata/WMAP7-CAMB.Step_0.01.HighAcc'
           elseif(ivar==2) then
              outfileCAMB='CAMBparamfiles/WMAP7-CAMB.Step_ByHand.HighAcc.'//trim(string(1:8))//'.ini'
              output_rootCAMB = 'CAMBdata/WMAP7-CAMB.Step_ByHand.HighAcc'
           endif
           
        endif
        write(*,'(a)') outfileCAMB
        
        ! SET THE CAMB PARAMETERS

        scalar_output_file        = 'ScalarCls.'//trim(string(1:8))//'.dat'
        vector_output_file        = 'VectorCls.'//trim(string(1:8))//'.dat'
        tensor_output_file        = 'TensorCls.'//trim(string(1:8))//'.dat'
        total_output_file         = 'TotalCls.'//trim(string(1:8))//'.dat'
        lensed_output_file        = 'LensedCls.'//trim(string(1:8))//'.dat'
        lensed_total_output_file  = 'LensedTotCls.'//trim(string(1:8))//'.dat'
        FITS_filename             = 'ScalarCls.'//trim(string(1:8))//'.fits'
        transfer_filename(1)      = 'TF.'//trim(string(1:8))//'.dat'
        transfer_matterpower(1)   = 'matterpower.'//trim(string(1:8))//'.dat' 

        open(1,file=outfileCAMB,status='unknown',form='formatted')     
        write(1,'(a30,a)')       'output_root                 = ',output_rootCAMB
        write(1,'(a30,a1)')      'get_scalar_cls              = ',get_scalar_cls
        write(1,'(a30,a1)')      'get_vector_cls              = ',get_vector_cls
        write(1,'(a30,a1)')      'get_tensor_cls              = ',get_tensor_cls
        write(1,'(a30,a1)')      'get_transfer                = ',get_transfer 
        write(1,'(a30,a1)')      'do_lensing                  = ',do_lensing
        write(1,'(a30,i1)')      'do_nonlinear                = ',do_nonlinear
        write(1,'(a30,i4)')      'l_max_scalar                = ',l_max_scalar
        write(1,'(a30,i5)')      'k_eta_max_scalar            = ',k_eta_max_scalar
        write(1,'(a30,i4)')      'l_max_tensor                = ',l_max_tensor
        write(1,'(a30,i4)')      'k_eta_max_tensor            = ',k_eta_max_tensor
        write(1,'(a30,a1)')      'use_physical                = ',use_physical
        write(1,'(a30,e14.7)')   'ombh2                       = ',ombh2
        write(1,'(a30,e14.7)')   'omch2                       = ',omch2
        write(1,'(a30,e14.7)')   'omnuh2                      = ',omnuh2
        write(1,'(a30,e14.7)')   'omk                         = ',omk
        write(1,'(a30,e14.7)')   'hubble                      = ',hubble
        write(1,'(a30,e14.7)')   'w                           = ',w
        write(1,'(a30,a1)')      'usew0wa                     = ',usew0wa
        write(1,'(a30,e14.7)')   'w0                          = ',w0
        write(1,'(a30,e14.7)')   'wa                          = ',wa
        write(1,'(a30,a60)')     'wafile                      = ',wafile
        write(1,'(a30,e14.7)')   'cs2_lam                     = ',cs2_lam
        write(1,'(a30,e14.7)')   'temp_cmb                    = ',temp_cmb
        write(1,'(a30,e14.7)')   'helium_fraction             = ',helium_fraction
        write(1,'(a30,e14.7)')   'massless_neutrinos          = ',massless_neutrinos
        write(1,'(a30,e14.7)')   'massive_neutrinos           = ',massive_neutrinos  
        write(1,'(a30,i1)')      'nu_mass_eigenstates         = ',nu_mass_eigenstates
        write(1,'(a30,i1)')      'nu_mass_degeneracies        = ',nu_mass_degeneracies
        write(1,'(a30,i1)')      'nu_mass_fractions           = ', nu_mass_fractions
        write(1,'(a30,i1)')      'initial_power_num           = ',initial_power_num
        write(1,'(a30,e14.7)')   'pivot_scalar                = ',pivot_scalar
        write(1,'(a30,e14.7)')   'pivot_tensor                = ',pivot_tensor
        write(1,'(a30,e14.7)')   'scalar_amp(1)               = ',scalar_amp(1)
        write(1,'(a30,e14.7)')   'scalar_spectral_index(1)    = ',scalar_spectral_index(1)
        write(1,'(a30,e14.7)')   'scalar_nrun(1)              = ',scalar_nrun(1)
        write(1,'(a30,e14.7)')   'tensor_spectral_index(1)    = ',tensor_spectral_index(1)
        write(1,'(a30,e14.7)')   'initial_ratio(1)            = ',initial_ratio(1)
        write(1,'(a30,a1)')      'reionization                = ',reionization
        write(1,'(a30,a1)')      're_use_optical_depth        = ',re_use_optical_depth
        write(1,'(a30,e14.7)')   're_optical_depth            = ',re_optical_depth
        write(1,'(a30,e14.7)')   're_redshift                 = ',re_redshift
        write(1,'(a30,e14.7)')   're_delta_redshift           = ',re_delta_redshift
        write(1,'(a30,e14.7)')   're_ionization_frac          = ',re_ionization_frac
        write(1,'(a30,e14.7)')   'RECFAST_fudge               = ',RECFAST_fudge
        write(1,'(a30,e14.7)')   'RECFAST_fudge_He            = ',RECFAST_fudge_He
        write(1,'(a30,i1)')      'RECFAST_Heswitch            = ',RECFAST_Heswitch
        write(1,'(a30,i1)')      'initial_condition           = ',initial_condition
        write(1,'(a30,5(i2,2x))')'initial_vector              = ',initial_vector(1:5)
        write(1,'(a30,i1)')      'vector_mode                 = ',vector_mode
        write(1,'(a30,a1)')      'COBE_normalize              = ',COBE_normalize
        write(1,'(a30,e14.7)')   'CMB_outputscale             = ',CMB_outputscale
        write(1,'(a30,1a)')      'transfer_high_precision     = ',transfer_high_precision
        write(1,'(a30,e14.7)')   'transfer_kmax               = ',transfer_kmax
        write(1,'(a30,i3)')      'transfer_k_per_logint       = ',transfer_k_per_logint
        write(1,'(a30,i2)')      'transfer_num_redshifts      = ',transfer_num_redshifts
        write(1,'(a30,a1)')      'transfer_interp_matterpower = ',transfer_interp_matterpower
        write(1,'(a30,e14.7)')   'transfer_redshift(1)        = ',transfer_redshift(1)
        write(1,'(a30,a)')       'transfer_filename(1)        = ',transfer_filename(1)
        write(1,'(a30,a)')       'transfer_matterpower(1)     = ',transfer_matterpower(1)
        write(1,'(a30,a)')       'scalar_output_file          = ',scalar_output_file
        write(1,'(a30,a)')       'vector_output_file          = ',vector_output_file
        write(1,'(a30,a)')       'tensor_output_file          = ',tensor_output_file
        write(1,'(a30,a)')       'total_output_file           = ',total_output_file
        write(1,'(a30,a)')       'lensed_output_file          = ',lensed_output_file
        write(1,'(a30,a)')       'lensed_total_output_file    = ',lensed_total_output_file
        write(1,'(a30,a)')       'FITS_filename               = ',FITS_filename
        write(1,'(a30,i1)')      'feedback_level              = ',feedback_level
        write(1,'(a30,i1)')      'lensing_method              = ',lensing_method
        write(1,'(a30,a1)')      'accurate_BB                 = ',accurate_BB
        write(1,'(a30,i1)')      'massive_nu_approx           = ',massive_nu_approx
        write(1,'(a30,a1)')      'accurate_polarization       = ',accurate_polarization
        write(1,'(a30,a1)')      'accurate_reionization       = ',accurate_reionization
        write(1,'(a30,a1)')      'do_tensor_neutrinos         = ',do_tensor_neutrinos
        write(1,'(a30,a1)')      'do_late_rad_truncation      = ',do_late_rad_truncation
        write(1,'(a30,i3)')      'number_of_threads           = ',number_of_threads
        write(1,'(a30,e14.7)')   'accuracy_boost              = ',accuracy_boost
        write(1,'(a30,e14.7)')   'l_accuracy_boost            = ',l_accuracy_boost
        write(1,'(a30,e14.7)')   'l_sample_boost              = ',l_sample_boost
        close(1)

        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        ! GENERATE 2LPT INPUT PARAMETERS

        !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        ! 2LPT OUTPUT FILES

        if(imodel==1) then
           
           if(ivar==1) then
              outfile2LPTtmp='2LPTparamfiles-L125-N512/2LPT-Planck2013.Step_0.01.HighAcc.'//trim(string(1:8))//'.ini'
           elseif(ivar==2) then
              outfile2LPTtmp='2LPTparamfiles-L125-N512/2LPT-Planck2013.Step_ByHand.HighAcc.'//trim(string(1:8))//'.ini'
           endif
           
        elseif(imodel==2) then
           
           if(ivar==1) then
              outfile2LPTtmp='2LPTparamfiles-L125-N512/2LPT-WMAP7-CAMB.Step_0.01.HighAcc.'//trim(string(1:8))//'.ini'
           elseif(ivar==2) then
              outfile2LPTtmp='2LPTparamfiles-L125-N512/2LPT-WMAP7-CAMB.Step_ByHand.HighAcc.'//trim(string(1:8))//'.ini'
           endif

        endif

        write(*,'(a)') outfile2LPTtmp

        omrad=0.0
        zFnl=0.0
        Fnl=0.0
        Nmesh=512
        zout=49.0
        Box=125.0


        NumFilesWrittenInParallel=16
        UnitLength_in_cm=3.085678d24  
        UnitMass_in_g=1.989d43     
        UnitVelocity_in_cm_per_s=1d5                

        ii=int(log10(Nmesh*1.0))
        if(ii==1) then
           write(FileBase,'(a,i2,a,i3,a)') 'Planck2013-Npart_',int(Nmesh),'_Box_',int(Box)
        elseif(ii==2) then
           write(FileBase,'(a,i3,a,i3,a)') 'Planck2013-Npart_',int(Nmesh),'_Box_',int(Box)
        elseif(ii==3) then
           write(FileBase,'(a,i4,a,i3,a)') 'Planck2013-Npart_',int(Nmesh),'_Box_',int(Box)
        elseif(ii==4) then
           write(FileBase,'(a,i5,a,i3,a)') 'Planck2013-Npart_',int(Nmesh),'_Box_',int(Box)
        endif

        write(*,'(a)') FileBase 

        GlassFile='./inputs/glass1_le'

        InputSpectrumFile=&
             '/hydra/u/rosm/NBODY/Daemmerung/CAMB/CAMBdata/Planck2013.Step_ByHand.HighAcc_matterpower.'&
             //trim(string(1:8))//'.dat'

        rootdir='/hydra/ptmp/rosm/Daemmerung/'

        MemFile='MEMORY-'//trim(FileBase)//'.dat'

        if(iDE1==1.or.iDE1==3) then
           var1='w0'
        elseif(iDE2==1.or.iDE2==3) then
           var1='w1'
        elseif(iomDE==1.or.iomDE==3) then
           var1='om_DE'
        elseif(iomch2==1.or.iomch2==3) then
           var1='omch2'
        elseif(iombh2==1.or.iombh2==3) then
           var1='ombh2'
        elseif(iScalSpec==1.or.iScalSpec==3) then
           var1='ns'
        elseif(iScalAmp==1.or.iScalAmp==3) then
           var1='As'
        elseif(iScalRun==1.or.iScalRun==3) then
           var1='running'
        else
           var1='-Fiducial'
        endif
        

        if(iDE1==1.or.iDE2==1.or.iomDE==1.or.iomch2==1.or.iombh2==1& 
             .or.iScalSpec==1.or.iScalAmp==1.or.iScalRun==1) then
           var2='_'//trim(var1)//'-'//'NegVar'
        elseif (iDE1==3.or.iDE2==3.or.iomDE==3.or.iomch2==3.or.iombh2==3& 
             .or.iScalSpec==3.or.iScalAmp==3.or.iScalRun==3) then
           var2='_'//trim(var1)//'-'//'PosVar'
        else
           var2=trim(var1)
        endif
        
        write(*,'(a)') outfile2LPTtmp

        do iensemb=1,nensemb

           OutputDir= trim(rootdir)//trim(FileBase)//trim(var2)//'/run'

           if(iensemb<10) then
              write(OutputDir,'(a,i1)') trim(OutputDir),iensemb
              write(outfile2LPT,'(a,a1,i1)') trim(outfile2LPTtmp),'.',iensemb
           elseif(iensemb>=10.and.iensemb<100) then
              write(OutputDir,'(a,i2)') trim(OutputDir),iensemb
              write(outfile2LPT,'(a,a1,i2)') trim(outfile2LPTtmp),'.',iensemb
           elseif(iensemb>=100) then
              write(OutputDir,'(a,i3)') trim(OutputDir),iensemb
              write(outfile2LPT,'(a,a1,i3)') trim(outfile2LPTtmp),'.',iensemb
           endif

           OutputDir=trim(OutputDir)//'/ICs2/'

           open(1,file=outfile2LPT,status='unknown',form='formatted')     
           write(1,'(a,5x,e14.7)') 'om_m0             ',omm
           write(1,'(a,5x,e14.7)') 'om_DE0            ',omde
           write(1,'(a,5x,e14.7)') 'om_b0             ',omb
           write(1,'(a,5x,e14.7)') 'om_rad0           ',omrad
           write(1,'(a,5x,e14.7)') 'om_nu0            ',omnu
           write(1,'(a,5x,e14.7)') 'w0                ',w0
           write(1,'(a,5x,e14.7)') 'w1                ',wa
           write(1,'(a,5x,e14.7)') 'hh                ',hubble
           write(1,'(a,5x,e14.7)') 'zFnl              ',zFNL
           write(1,'(a,5x,e14.7)') 'Fnl               ',FNL
           write(1,'(a,5x,i8)')    'Nmesh             ',Nmesh
           write(1,'(a,5x,e14.7)') 'zout              ',zout
           write(1,'(a,5x,e14.7)') 'Box               ',Box
           write(1,'(a,5x,a)')     'MemFile           ',MemFile
           write(1,'(a,5x,a)')     'FileBase          ',trim(FileBase)//trim(var2)
           write(1,'(a,5x,a)')     'OutputDir         ',OutputDir
           write(1,'(a,5x,a)')     'GlassFile         ',GlassFile
           write(1,'(a,5x,i8)')    'GlassTileFac      ',GlassTileFac
           write(1,'(a,5x,a)')     'InputSpectrumFile ',InputSpectrumFile
           write(1,'(a,5x,i8)')    'Seed              ',Seed(iensemb)
           write(1,'(a,5x,i8)')    'NumFilesWrittenInParallel',NumFilesWrittenInParallel
           write(1,'(a,5x,e14.7)') 'UnitLength_in_cm  ',UnitLength_in_cm
           write(1,'(a,5x,e14.7)') 'UnitMass_in_g     ',UnitMass_in_g
           write(1,'(a,5x,e14.7)') 'UnitVelocity_in_cm_per_s',UnitVelocity_in_cm_per_s
           close(1)

        enddo
           
     endif
     
  enddo
  enddo
  enddo
  enddo
  enddo
  enddo
  enddo
  enddo

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end program CAMB_2LPT_GenParamFiles

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
