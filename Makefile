
#SRC = CAMBGenParamFiles.1

SRC = CAMB-2LPT-GenParamFiles.2

F90 = gfortran

F90FLAGS = -O3 -ffixed-line-length-none

OBJ = ${SRC}.o

EXECUTE = ${SRC}.exe

INCL = 

LIB1 = -L/users/res/pgplot5.2/pgplotlibGFORTRAN64 -lpgplot
LIB2 = -lf2c -L/usr/X11R6/lib -lX11  -lgcc -lm -lc 

LIBS = 

all: ${EXECUTE}

${EXECUTE}: ${OBJ}
	${F90} ${F90LAGS} ${SRC}.o -o ${SRC}.exe ${LIBS}

${OBJ}: ${SRC}.f90
	${F90} ${INCL} ${F90LAGS} -c ${SRC}.f90

clean:
	rm *.o *.exe
